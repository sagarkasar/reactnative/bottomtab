import React from "react";
import NavigationStrings from "../Constants/NavigationStrings";
import TabRoutes from "./TabRoutes";
import { ProductDetails, EditAccount, EditProfile } from "../Screens";


export default function (Stack:any){
    return(
        <>
        <Stack.Screen 
        name={NavigationStrings.TAB}
        component={TabRoutes}
        />
        
        </>
    )
}