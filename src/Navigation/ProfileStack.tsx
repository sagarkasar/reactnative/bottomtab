import React from "react";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import NavigationStrings from "../Constants/NavigationStrings";
import { EditProfile, Profile } from "../Screens";

const  Stack= createNativeStackNavigator();

export default function ProfileStack(){
    return(
        <Stack.Navigator>
            <Stack.Screen name={NavigationStrings.PROFILE} component={Profile}/>
            <Stack.Screen name={NavigationStrings.EDITPROFILE} component={EditProfile}/>
        </Stack.Navigator>
    )
}