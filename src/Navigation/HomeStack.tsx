import React from "react";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import NavigationStrings from "../Constants/NavigationStrings";
import { Home, ProductDetails } from "../Screens";

const  Stack= createNativeStackNavigator();

export default function HomeStack(){
    return(
        <Stack.Navigator>
            <Stack.Screen name={NavigationStrings.HOME} component={Home}/>
            <Stack.Screen name={NavigationStrings.PRODUCTDETAILS} component={ProductDetails}/>
        </Stack.Navigator>
    )
}