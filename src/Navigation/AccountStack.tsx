import React from "react";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import NavigationStrings from "../Constants/NavigationStrings";
import { Account, EditAccount, EditProfile } from "../Screens";

const  Stack= createNativeStackNavigator();

export default function AccountStack(){
    return(
        <Stack.Navigator>
            <Stack.Screen name={NavigationStrings.ACCOUNT} component={Account}/>
            <Stack.Screen name={NavigationStrings.EDITACCOUNT} component={EditAccount}/>
            <Stack.Screen name={NavigationStrings.EDITPROFILE} component={EditProfile}/>
        </Stack.Navigator>
    )
}