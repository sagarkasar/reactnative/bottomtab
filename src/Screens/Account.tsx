import React, { Component } from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';
import NavigationStrings from '../Constants/NavigationStrings';


const Account = ({navigation}: any) => {
    const GotoAccount = () =>{
        navigation.navigate(NavigationStrings.EDITACCOUNT)
    } 
    return (
        <View style={styles.container}>
            <Text>Acoount Components</Text>
            <Button title='Edit Account' onPress={GotoAccount}/>
            <Button title='Edit Profile' onPress={() =>navigation.navigate(NavigationStrings.EDITPROFILE)}/>
        </View>
    )
}

export default Account;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});