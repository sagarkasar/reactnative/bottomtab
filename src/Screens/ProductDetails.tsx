import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const ProductDetails = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.text}>Product Details Page</Text>
        </View>
    )
}

export default ProductDetails

const styles = StyleSheet.create({
    container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text:{
      color:'red',
      fontSize:25
  }
})
