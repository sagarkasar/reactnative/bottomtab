import React, { Component } from 'react'
import { Button, StyleSheet, Text, View } from 'react-native';
import NavigationStrings from '../Constants/NavigationStrings';

const Profile = ({navigation}: any) => {
    return (
        <View style={styles.container}>
            <Text>Profile Components</Text>
            <Button title='Edit Profile' onPress={() =>navigation.navigate(NavigationStrings.EDITPROFILE)}/>
        </View>
    )
}

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});